//
//  IGSBasketHeader.h
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 19.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGSBasketHeader : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *totalProducLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@end
