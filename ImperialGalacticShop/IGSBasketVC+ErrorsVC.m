//
//  IGSBasketVC+ErrorsVC.m
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 22.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSBasketVC+ErrorsVC.h"

@implementation IGSBasketVC (ErrorsVC)

-(void)viewDidLoad
{
    [self.navigationItem setTitle:@"Error"];
    self.tableView = [[UITableView alloc]initWithFrame:self.tableView.frame style:UITableViewStyleGrouped];
}

@end
