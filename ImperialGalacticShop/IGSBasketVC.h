//
//  IGSBasketVC.h
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 19.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGSBasketVC : UITableViewController

@property (strong,nonatomic) NSMutableArray *localBasket;
@property (assign,nonatomic) BOOL isFixMode;
@property (strong,nonatomic) NSDictionary *errorsDictionary;

@end
