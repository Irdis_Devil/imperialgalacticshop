//
//  IGSBasketVC.m
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 19.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSBasketVC.h"
#import "IGSBasketHeader.h"
#import "IGSGeneralTableVC.h"
#import "AdministratorCell.h"
#import "IGSSingleton.h"
#import "Product.h"
#import "ShopCellEntity.h"
#import "IGSProductDetailVC.h"
#import "IGSOperation.h"
#import "IGSErrorBasketTableViewCell.h"

static NSString *headerIdentifier = @"headerIdf";

@interface IGSBasketVC () <UIAlertViewDelegate, BasketCellDelegate>
{
    UIView *_myBlockView;
}

@end

@implementation IGSBasketVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINib *header = [UINib nibWithNibName:@"BasketHeaderView" bundle:nil];
    [self.tableView registerNib:header forHeaderFooterViewReuseIdentifier:headerIdentifier];
    if (_isFixMode) [self fixModeSettings];
}

-(void)fixModeSettings
{
    [self.navigationItem setTitle:@"Error"];
    UIBarButtonItem *closeItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closeMe)];
    [self.navigationItem setRightBarButtonItem:closeItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    //я считаю, что тут данный ход актуальней, так как выход из корзины и наоборот подразумевает изменения в ее составляющих.
    [self.tableView reloadData];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(buyReturnErrorWithArray:) name:@"Buy_Is_Crashed" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(buyComplete) name:@"Buy_Completed" object:nil];
}

//-(void)viewWillDisappear:(BOOL)animated
//{
//    [[NSNotificationCenter defaultCenter]removeObserver:self];
//}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 88;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    IGSBasketHeader *basketHeader = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerIdentifier];
    basketHeader.totalProducLabel.text = [NSString stringWithFormat:@"%i",_localBasket.count];
    basketHeader.totalPriceLabel.text = [NSString stringWithFormat:@"%.2f",[IGSMediator calculateTotalPriceAtArraProducts:_localBasket]];
    [basketHeader.clearButton addTarget:self action:@selector(clearBasketButtonWasPressed) forControlEvents:UIControlEventTouchUpInside];
    [basketHeader.buyButton addTarget:self action:@selector(buyButtonWasPressed) forControlEvents:UIControlEventTouchUpInside];
    return basketHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (_isFixMode)? 84 : 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _localBasket.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Product *prod = _localBasket[indexPath.row];
    if (_isFixMode) {
        IGSErrorBasketTableViewCell *errorCell = [tableView dequeueReusableCellWithIdentifier:@"ErrorCell"];
        errorCell.productNameLable.text = prod.nameProduct;
        errorCell.productPriceLabel.text = prod.price.stringValue;
        errorCell.productCountLabel.text = prod.countProduct.stringValue;
        errorCell.delegate = self;
        [errorCell.stepper setMaximumValue:prod.countProduct.doubleValue];
        [errorCell.stepper setValue:(_isFixMode)?[[self calculateQuantityInBasketAtProduct:prod]doubleValue] : prod.quantityInBasket.doubleValue];
        errorCell.quiantityInBasketLabel.text = (_isFixMode)?[[self calculateQuantityInBasketAtProduct:prod]stringValue] : prod.quantityInBasket.stringValue;
    return errorCell;
    } else {
        AdministratorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdministratorCell"];
        cell.countLabel.text = prod.quantityInBasket.stringValue;
        cell.titleLabel.text = prod.nameProduct;
        cell.subtitleLabel.text = prod.price.stringValue;
        return cell;
    }
}

-(NSNumber *)calculateQuantityInBasketAtProduct:(Product*)product
{
    NSNumber *returningNumber;
    if ([[_errorsDictionary valueForKey:product.identifier] intValue] > product.countProduct.intValue){
        returningNumber = [NSNumber numberWithInt:product.countProduct.intValue];
    } else returningNumber = [NSNumber numberWithInt:[[_errorsDictionary valueForKey:product.identifier] intValue]];
    product.quantityInBasket = returningNumber;
    return returningNumber;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Product *prod = _localBasket[indexPath.row];
        prod.quantityInBasket = @(0);
        [[NSManagedObjectContext defaultContext] saveToPersistentStoreWithCompletion:nil];
        if ([_localBasket containsObject:prod])[_localBasket removeObject:prod];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

-(void)clearBasketButtonWasPressed
{
    [_localBasket removeAllObjects];
    [self.tableView reloadData];
    [IGSMediator deleteAllCellEntities];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"BasketDataWasChanged"];
}

-(void)buyButtonWasPressed
{
    if (_localBasket.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Hey!" message:@"Your basket is empty!" delegate:self cancelButtonTitle:@"Ouch!" otherButtonTitles: nil];
        [alert show];
    } else
    {
        IGSOperation *newOperation = [IGSOperation new];
        newOperation.operationKey = @"BuyAction";
        NSMutableDictionary *dicts = [NSMutableDictionary dictionary];
        for (Product *prod in _localBasket) {
            [dicts setObject:prod forKey:prod.quantityInBasket];
            prod.quantityInBasket = @(0);
        }
        newOperation.changes = dicts;
        [[IGSSingleton sharedInstanse]addNewOperationInOperationStack:newOperation];
        [IGSMediator runOperationBlock];
        [_localBasket removeAllObjects];
        [self.tableView reloadData];
        [IGSMediator deleteAllCellEntities];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"BasketDataWasChanged"];
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Detail" sender:_localBasket[indexPath.row]];
}

-(void)stepperChangeValueInCell:(UITableViewCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [_localBasket[indexPath.row] setValue:@([[(IGSErrorBasketTableViewCell*)cell stepper] value])  forKeyPath:@"quantityInBasket"];
    [[(IGSBasketHeader*)[self.tableView headerViewForSection:0] totalProducLabel] setText:[NSString stringWithFormat:@"%i",_localBasket.count]];
    [[(IGSBasketHeader*)[self.tableView headerViewForSection:0] totalPriceLabel] setText:[NSString stringWithFormat:@"%.2f",[IGSMediator calculateTotalPriceAtArraProducts:_localBasket]]];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IGSProductDetailVC *productDetailVC = [segue destinationViewController];
    productDetailVC.isAdminMode = NO;
    if ([segue.identifier isEqualToString:@"Detail"]) {
        productDetailVC.localObject = (Product*)sender;
    }
}

-(void)closeMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
