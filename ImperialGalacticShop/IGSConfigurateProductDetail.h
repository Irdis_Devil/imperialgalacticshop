//
//  IGSConfigurateProductDetail.h
//  ImperialGalacticShop
//
//  Created by Администратор on 5/14/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface IGSConfigurateProductDetail : NSObject

+(NSArray*)getCurrentSectionAtMode:(BOOL)isAdminMode andProduct:(Product*)product;

@end
