//
//  IGSConfigurateProductDetail.m
//  ImperialGalacticShop
//
//  Created by Администратор on 5/14/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSConfigurateProductDetail.h"
#import "ProductDetailSection.h"

#define GET_ALL_SECTION_SORTED_BY_ORDER [ProductDetailSection MR_findAllSortedBy:@"sectionOrder" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"sectionShowOnlyUser == NO || sectionShowOnlyUser == nil"]]
#define GET_PARAMETER_REQUIRED @"sectionRequired == YES"
#define GET_PARAMETER_NOT_REQUIRED_OR_NIL @"sectionRequired == NO || sectionRequired == nil &&  (sectionShowOnlyAdmin != YES || sectionShowOnlyAdmin == nil)"

@implementation IGSConfigurateProductDetail

+(NSArray *)getCurrentSectionAtMode:(BOOL)isAdminMode andProduct:(Product*)product
{
    if (isAdminMode) {
        return [NSArray arrayWithArray:GET_ALL_SECTION_SORTED_BY_ORDER];
    } else {
        NSMutableArray *compositeArray = [NSMutableArray arrayWithArray:[ProductDetailSection MR_findAllWithPredicate:[NSPredicate predicateWithFormat:GET_PARAMETER_REQUIRED]]];
        NSArray *notRequierdSection = [ProductDetailSection MR_findAllWithPredicate:[NSPredicate predicateWithFormat:GET_PARAMETER_NOT_REQUIRED_OR_NIL]];
        for (ProductDetailSection *section in notRequierdSection) {
            if ([self checkProductPropertyData:product fromSection:section]) {
                [compositeArray addObject:section];
            }
        }
        [compositeArray sortUsingComparator:^NSComparisonResult(ProductDetailSection *obj1, ProductDetailSection *obj2) {
            return [obj1.sectionOrder compare:obj2.sectionOrder];
        }];
        return compositeArray;
    }
}

+(BOOL)checkProductPropertyData:(Product*)product fromSection:(ProductDetailSection*)section
{
    BOOL result = NO;
        if ([product valueForKey:section.sectionIdentifier] != nil || ![[product valueForKey:section.sectionIdentifier] isEqualToString:@""]){
            result = YES;
        }
    return result;
}


@end
