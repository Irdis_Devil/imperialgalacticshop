//
//  IGSErrorBasketTableViewCell.h
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 23.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BasketCellDelegate <NSObject>;

-(void)stepperChangeValueInCell:(UITableViewCell*)cell;

@end

@interface IGSErrorBasketTableViewCell : UITableViewCell

@property (weak, nonatomic) id <BasketCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *productNameLable;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *quiantityInBasketLabel;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;

@end
