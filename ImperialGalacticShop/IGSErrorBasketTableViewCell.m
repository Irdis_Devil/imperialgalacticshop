//
//  IGSErrorBasketTableViewCell.m
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 23.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSErrorBasketTableViewCell.h"

@implementation IGSErrorBasketTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)stepperValueChanged:(id)sender {
    self.quiantityInBasketLabel.text = [NSString stringWithFormat:@"%i",(int)self.stepper.value];
    [self.delegate performSelector:@selector(stepperChangeValueInCell:) withObject:self];
}

@end
