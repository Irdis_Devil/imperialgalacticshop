//
//  IGSGeneralTableVC.m
//  ImperialGalacticShop
//
//  Created by Алексей on 13.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSGeneralTableVC.h"
#import "Product.h"
#import "ShopCell.h"
#import "AdministratorCell.h"
#import "IGSProductDetailVC.h"
#import "ShopCellEntity.h"
#import "IGSSingleton.h"
#import "IGSBasketVC.h"

#define GET_SHOP_CELL_ENTITY_AT_INDEXPATH(index) [ShopCellEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"indexRow.integerValue == %i",index]]

@interface IGSGeneralTableVC () <ShopCellDelegate>
{
    NSMutableArray *_localObjects;
    NSMutableArray *_prodToBasket;
}
@property (assign,nonatomic) BOOL isAdminMode;
@property IGSMediator *mediator;

@end

static NSString *shopCellId = @"ShopCell";
static NSString *adminCellId = @"AdministratorCell";

@implementation IGSGeneralTableVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isAdminMode = NO;
    _prodToBasket = [NSMutableArray array];
    [self setNavBar];
    [self prepareTableViewWithCreateCellEntities:YES];
}

-(void)setNavBar
{
    self.navigationItem.title = (_isAdminMode) ? @"Admin Mode" : @"Shop";
    UIBarButtonItem *addNew = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(preparePerformSelector:)];
    UIBarButtonItem *basketBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(showBasket)];
    [self reloadNavBarWithRightButton:(_isAdminMode) ? addNew : basketBtn];
}

-(void)prepareTableViewWithCreateCellEntities:(BOOL)createEntities
{
    _localObjects = [NSMutableArray arrayWithArray:[Product MR_findAllSortedBy:@"identifier" ascending:YES]];
    //сразу создадим все сущности для ячеек.
    if (createEntities) {
        for (int i = 0; i<_localObjects.count; i++) {
            ShopCellEntity *cellEntity = [ShopCellEntity MR_createEntity];
            cellEntity.indexRow = @(i);
            cellEntity.stepperValue = @(0);
            cellEntity.cellSelected = @(NO);
        }
    }
}

-(void)reloadNavBarWithRightButton:(UIBarButtonItem*)rightBarButton
{
    self.navigationItem.rightBarButtonItem = rightBarButton;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    //маркер, чтобы перезагрузить данные в таблице
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"DataBaseWasChanged"]) {
        [self prepareTableViewWithCreateCellEntities:NO];
        [self.tableView reloadData];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DataBaseWasChanged"];
    }
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"BasketDataWasChanged"]){
        [self prepareTableViewWithCreateCellEntities:YES];
        [self.tableView reloadData];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"BasketDataWasChanged"];
    }
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _localObjects.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (_isAdminMode) ? 50 : 170;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        AdministratorCell *cell = (AdministratorCell*)[tableView dequeueReusableCellWithIdentifier:(_isAdminMode) ? adminCellId : shopCellId forIndexPath:indexPath];
        [self configureCell:cell atIndexPath:indexPath];
    
        return cell;
}

-(void)configureCell:(id)cell atIndexPath:(NSIndexPath*)indexPath
{
    //смотрим, от какого класса ячейка и настраиваем ее
    Product* prod = _localObjects[indexPath.row];
    if ([cell isKindOfClass:[ShopCell class]]) {
        ShopCell *tcell = (ShopCell*)cell;
        tcell.delegate = self;
        tcell.tag  = indexPath.row;

        ShopCellEntity *cellEntity = GET_SHOP_CELL_ENTITY_AT_INDEXPATH(indexPath.row);
        if (cellEntity.cellSelected.boolValue){
            [tcell setSelected:YES];
            [tcell.stepper setEnabled:NO];
            [tcell setAccessoryType:UITableViewCellAccessoryCheckmark];
        } else {
            [tcell setAccessoryType:UITableViewCellAccessoryNone];
            [tcell.stepper setEnabled:YES];
            [tcell setSelected:NO];
        }
        tcell.stepper.value = cellEntity.stepperValue.doubleValue;
        tcell.countToBasket.text = cellEntity.stepperValue.stringValue;
        
        tcell.productNameLabel.text = prod.nameProduct;
        tcell.countLabel.text = prod.countProduct.stringValue;
        tcell.manufacturerLabel.text = prod.manufacturer;
        tcell.priceLabel.text = prod.price.stringValue;
        [tcell.productImage setImage:[IGSHelper getCurrentImageAtName:prod.imageName]];
        [tcell setMaxValueToStepper:prod.countProduct.doubleValue];
        [tcell.showDetailButton setTag:indexPath.row];
        [tcell.showDetailButton addTarget:self action:@selector(preparePerformSelector:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        AdministratorCell *tcell = (AdministratorCell*)cell;
        tcell.titleLabel.text = prod.nameProduct;
        tcell.subtitleLabel.text = prod.identifier;
        tcell.countLabel.text = prod.countProduct.stringValue;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (_isAdminMode);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
#warning Удаление в медиатор! 
        Product *prodToDelete = _localObjects[indexPath.row];
        [prodToDelete MR_deleteEntity];
        ShopCellEntity *cellEntity = GET_SHOP_CELL_ENTITY_AT_INDEXPATH(indexPath.row);
        [cellEntity MR_deleteEntity];
        [[NSManagedObjectContext defaultContext]saveToPersistentStoreWithCompletion:nil];
        [_localObjects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {

    }   
}

//ну и как обычно
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isAdminMode) {
        [self performSegueWithIdentifier:@"Detail" sender:_localObjects[indexPath.row]];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    } else {
        ShopCell *cell = (ShopCell*)[tableView cellForRowAtIndexPath:indexPath];
        if (cell.stepper.value !=0) {
            ShopCellEntity *cellEntity = GET_SHOP_CELL_ENTITY_AT_INDEXPATH(indexPath.row);
            cellEntity.cellSelected = @(YES);
            cellEntity.stepperValue = @(cell.stepper.value);
            [cell.stepper setEnabled:NO];
            [self addToBasketProductAtIndex:indexPath.row withQuantityInBasket:cellEntity.stepperValue.integerValue];
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        } else {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [UIView animateWithDuration:0.5 animations:^{
                cell.countToBasket.backgroundColor = [UIColor redColor];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:1 animations:^{
                    cell.countToBasket.backgroundColor = [UIColor clearColor];
                }];
            }];
        }
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_isAdminMode) {
         ShopCellEntity *cellEntity = GET_SHOP_CELL_ENTITY_AT_INDEXPATH(indexPath.row);
        cellEntity.cellSelected = @(NO);
        [self deleteProductAtIndexFromBasket:indexPath.row];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    }
}

-(void)addToBasketProductAtIndex:(NSInteger)index withQuantityInBasket:(NSInteger)quantity
{
    Product *productToBasket = _localObjects[index];
    productToBasket.quantityInBasket = @(quantity);
    [_prodToBasket addObject:productToBasket];
}

-(void)deleteProductAtIndexFromBasket:(NSInteger)index
{
    Product *productToBasket = _localObjects[index];
    productToBasket.quantityInBasket = @(0);
    if ([_prodToBasket containsObject:productToBasket]) [_prodToBasket removeObject:productToBasket];
}

-(void)stepperChangeValueInCell:(UITableViewCell*)cell
{
    ShopCellEntity *cellEntity = GET_SHOP_CELL_ENTITY_AT_INDEXPATH(cell.tag);
    cellEntity.stepperValue = @([[(ShopCell*)cell stepper]value]);
}

-(void)stepperValueIsNullinCell:(UITableViewCell *)cell
{
    ShopCellEntity *cellEntity = GET_SHOP_CELL_ENTITY_AT_INDEXPATH(cell.tag);
    cellEntity.stepperValue = @(0);
    cellEntity.cellSelected = @(NO);
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:cell.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark -
#pragma mark - Navigation

-(void)showBasket
{
    [self performSegueWithIdentifier:@"showBasket" sender:self];
}

-(void)preparePerformSelector:(id)sender
{
    if (_isAdminMode){
        [self performSegueWithIdentifier:@"AddNewProduct" sender:sender];
    } else {
        UIButton *btn = (UIButton*)sender;
        [self performSegueWithIdentifier:@"Detail" sender:_localObjects[btn.tag]];
    }
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IGSProductDetailVC *productDetailVC = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"Detail"]) {
         productDetailVC.isAdminMode = _isAdminMode;
        productDetailVC.localObject = (Product*)sender;
    } else if ([segue.identifier isEqualToString:@"AddNewProduct"]){
        productDetailVC.localObject = [Product MR_createEntity];
        productDetailVC.isAdminMode = _isAdminMode;
        productDetailVC.isAdminCreateProductMode = YES;
    } else if ([segue.identifier isEqualToString:@"showBasket"]){
        IGSBasketVC *basketVC = [segue destinationViewController];
        basketVC.localBasket = _prodToBasket;
    }
}

#pragma mark -
#pragma mark - Application mode
- (IBAction)changeMode:(id)sender
{
    _isAdminMode = !_isAdminMode;
    [_prodToBasket removeAllObjects];
    [self setNavBar];
    if (_isAdminMode) {
        [IGSMediator deleteAllCellEntities];
    } else {
        [self prepareTableViewWithCreateCellEntities:YES];
    }
    [self.tableView reloadData];
}

@end
