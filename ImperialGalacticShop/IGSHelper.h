//
//  IGSHelper.h
//  ImperialGalacticShop
//
//  Created by Администратор on 5/14/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGSHelper : NSObject

+(UIImage*)getCurrentImageAtName:(NSString*)imagename;
+(UIViewController*)topMostController;

@end
