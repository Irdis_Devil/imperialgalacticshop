//
//  IGSHelper.m
//  ImperialGalacticShop
//
//  Created by Администратор on 5/14/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSHelper.h"

@implementation IGSHelper

+(UIImage *)getCurrentImageAtName:(NSString *)imagename
{
    if (imagename !=nil && ![imagename isEqualToString:@""]) {
        UIImage *temporaryImage = [UIImage imageNamed:imagename];
        if (temporaryImage != nil) {
            return temporaryImage;
        }
    }
    return [UIImage imageNamed: @"noImage.jpeg"];
}

+(UIViewController*)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}
@end
