//
//  IGSMediator.h
//  ImperialGalacticShop
//
//  Created by Администратор on 5/19/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface IGSMediator : NSObject 

+(void)deleteAllCellEntities;
+(void)runOperationBlock;

+(float)calculateTotalPriceAtArraProducts:(NSArray*)arrayProducts;

@end
