
//
//  IGSMediator.m
//  ImperialGalacticShop
//
//  Created by Администратор on 5/19/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSMediator.h"
#import "ShopCellEntity.h"
#import "ProductDetailSection.h"
#import "IGSSingleton.h"
#import "IGSOperation.h"
#import "IGSBasketVC.h"

#define SLEEP_TIME 8.05

@implementation IGSMediator

+(void)deleteAllCellEntities
{
    NSMutableArray *objectsToDelete = [NSMutableArray arrayWithArray:[ShopCellEntity MR_findAll]];
    for (ShopCellEntity *shopCell in objectsToDelete) {
        [shopCell MR_deleteEntity];
    }
}

+(void)runOperationBlock
{
    //чтобы не перезапускать все по десять раз
    if ([[IGSSingleton sharedInstanse] isAllOperationDone]){
        dispatch_async([[IGSSingleton sharedInstanse]getSingletonQueue], ^{
            while ([[IGSSingleton sharedInstanse]getOperationArrayCount] !=0){
                NSLog(@"\n\n startOperation \n\n");
                IGSOperation *operation = [[IGSSingleton sharedInstanse] getFirstOperationFromStack];//[i];
                [NSThread sleepForTimeInterval:SLEEP_TIME];
                NSLog(@"\n\n after sleep \n\n");
                if ([operation.operationKey isEqualToString:@"ChangeOrCreate"]){
                    [self changeProductParametersOrCreateAtOperation:operation];
                } else {
                    [self buyWasStartWithOperation:operation];
                }
            }
            NSLog(@"\n\n done All \n\n");
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            });
        });
    }
}

+(void)changeProductParametersOrCreateAtOperation:(IGSOperation*)operation
{
    [operation.product setValuesForKeysWithDictionary:operation.parameters];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DataBaseWasChanged"];
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreWithCompletion:nil];
    NSLog(@"\n\n done \n\n");
}

+(void)buyWasStartWithOperation:(IGSOperation*)operation
{
    NSMutableDictionary *errorsProduct = [NSMutableDictionary dictionary];
    NSMutableDictionary *newValue = [NSMutableDictionary dictionary];
    BOOL isWasError = NO;
        for (id key in operation.changes.allKeys) {
            Product *currentProduct = [operation.changes objectForKey:key];
        if ([currentProduct.countProduct intValue] >= [key intValue]){
            [newValue setValue:@(currentProduct.countProduct.intValue - [key intValue]) forKey:currentProduct.identifier];
        }
        else{
            isWasError = YES;
        }
        [errorsProduct setValue:key forKey: currentProduct.identifier];
    }
    if (isWasError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            IGSBasketVC *vc = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"basketVC"];
            vc.isFixMode = YES;
            vc.localBasket =[NSMutableArray arrayWithArray:[operation.changes allValues]];
            vc.errorsDictionary = errorsProduct;
            UINavigationController *navContr = [[UINavigationController alloc]initWithRootViewController:vc];
            [[IGSHelper topMostController] presentViewController:navContr animated:YES completion:^{
                
            }];
        });
    }else {
        for (NSString *key in newValue.allKeys) {
            Product * newProd = [Product MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"identifier = %@",key]];
            newProd.countProduct =[newValue valueForKey:key];
        }
        [[NSManagedObjectContext MR_contextForCurrentThread] saveToPersistentStoreWithCompletion:nil];
    }
    NSLog(@"\n\n done \n\n");
}

+(float)calculateTotalPriceAtArraProducts:(NSArray *)arrayProducts
{
    float result = 0;
    for (Product *prod in arrayProducts) {
        result = result + prod.price.floatValue * prod.quantityInBasket.floatValue;
    }
    return result;
}

@end
