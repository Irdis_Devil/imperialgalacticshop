//
//  IGSOperation.h
//  Project
//
//  Created by Алексей on 06.05.14.
//  Copyright (c) 2014 Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface IGSOperation : NSObject <NSCoding>

@property (strong,nonatomic) NSString *operationKey;
@property (strong,nonatomic) Product *product;
@property (strong,nonatomic) NSDictionary *parameters;
@property (strong,nonatomic) NSDictionary *changes;

-(id)initWithOperation:(IGSOperation*)operation;

@end
