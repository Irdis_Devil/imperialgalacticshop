//
//  PROperation.m
//  Project
//
//  Created by Алексей on 06.05.14.
//  Copyright (c) 2014 Pro. All rights reserved.
//

#import "IGSOperation.h"

@implementation IGSOperation

-(id)initWithOperation:(IGSOperation*)operation
{
    self = operation;
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.parameters = [aDecoder decodeObjectForKey:@"OperationParameter"];
        self.operationKey = [aDecoder decodeObjectForKey:@"OperationKey"];
        self.product = [aDecoder decodeObjectForKey:@"OperationProd"];
        self.changes = [aDecoder decodeObjectForKey:@"OperationProdArr"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.parameters forKey:@"OperationParameter"];
    [aCoder encodeObject:self.operationKey forKey:@"OperationKey"];
    [aCoder encodeObject:self.product forKey:@"OperationProd"];
    [aCoder encodeObject:self.changes forKey:@"OperationProdArr"];
}
@end
