//
//  IGSPreFillDataBase.h
//  ImperialGalacticShop
//
//  Created by Алексей on 13.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGSPreFillDataBase : NSObject

+(void)prefillDatabase;

@end
