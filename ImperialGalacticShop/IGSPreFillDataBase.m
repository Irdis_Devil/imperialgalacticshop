//
//  IGSPreFillDataBase.m
//  ImperialGalacticShop
//
//  Created by Алексей on 13.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSPreFillDataBase.h"
#import "Product.h"
#import "ProductDetailSection.h"

@implementation IGSPreFillDataBase
+(void)prefillDatabase
{
    //начнем прописывать продукты
    Product *prod1 = [Product MR_createEntity];
    prod1.nameProduct = @"Eclipse - class Star Destroyer";
    prod1.identifier = @"Eclipse";
    prod1.manufacturer = @"Kuat Drive Yards";
    prod1.crew = @(708470);
    prod1.lenght = @(17500);
    prod1.autonomy = @(10);
    prod1.quantityInBasket = @(0);
    prod1.imageName = @"Eclipse.jpg";
    prod1.urlAddress = @"";
    prod1.price = @(2878.46f);
    prod1.tonnage = @(600000);
    prod1.countProduct = @(23);
    
    Product *prod2 = [Product MR_createEntity];
    prod2.nameProduct = @"Executor - class Super Star Destroyer";
    prod2.identifier = @"Executor";
    prod2.manufacturer = @"Kuat Drive Yards";
    prod2.crew = @(280734);
    prod2.lenght = @(19000);
    prod2.autonomy = @(6);
    prod2.quantityInBasket = @(0);
    prod2.imageName = @"Executor.jpg";
    prod2.urlAddress = @"";
    prod2.price = @(1143.35f);
    prod2.tonnage = @(250000);
    prod2.countProduct = @(34);
    
    Product *prod3 = [Product MR_createEntity];
    prod3.nameProduct = @"Interdictor - class Heavy Cruiser";
    prod3.identifier = @"Interdictor";
    prod3.manufacturer = @"Sienar Fleet Systems";
    prod3.crew = @(2807);
    prod3.lenght = @(600);
    prod3.autonomy = @(1.2f);
    prod3.quantityInBasket = @(0);
    prod3.imageName = @"Interdictor.jpg";
    prod3.urlAddress = @"";
    prod3.price = @(52.24f);
    prod3.tonnage = @(5500);
    prod3.countProduct = @(45);
    
    Product *prod4 = [Product MR_createEntity];
    prod4.nameProduct = @"Imperator I - class Star Destroyer";
    prod4.identifier = @"Imperator I";
    prod4.manufacturer = @"Kuat Drive Yards";
    prod4.crew = @(37085);
    prod4.lenght = @(1600);
    prod4.autonomy = @(6);
    prod4.quantityInBasket = @(0);
    prod4.imageName = @"Imperator_I.jpg";
    prod4.urlAddress = @"";
    prod4.price = @(130);
    prod4.tonnage = @(36000);
    prod4.countProduct = @(56);
    
    Product *prod5 = [Product MR_createEntity];
    prod5.nameProduct = @"Sovereign - class Star Destroyer";
    prod5.identifier = @"Sovereign";
    prod5.manufacturer = @"Kuat Drive Yards";
    prod5.crew = @(605.745);
    prod5.lenght = @(15000);
    prod5.autonomy = @(5);
    prod5.quantityInBasket = @(0);
    prod5.imageName = @"Sovereign.jpg";
    prod5.urlAddress = @"";
    prod5.price = @(1931.28f);
    prod5.tonnage = @(400000);
    prod5.countProduct = @(67);
    
    //теперь секции
    ProductDetailSection *section1 = [ProductDetailSection MR_createEntity];
    section1.sectionTitle = @"Product name:";
    section1.sectionCellQuantity = @(1);
    section1.sectionOrder = @(0);
    section1.sectionIdentifier = @"nameProduct";
    section1.sectionRequired = @(YES);
    
    ProductDetailSection *section2 = [ProductDetailSection MR_createEntity];
    section2.sectionTitle = @"Manufacturer:";
    section2.sectionCellQuantity = @(1);
    section2.sectionOrder = @(1);
    section2.sectionIdentifier = @"manufacturer";
    
    ProductDetailSection *section3 = [ProductDetailSection MR_createEntity];
    section3.sectionTitle = @"Price (million credit):";
    section3.sectionCellQuantity = @(1);
    section3.sectionOrder = @(2);
    section3.sectionIdentifier = @"price";
    section3.sectionRequired = @(YES);
    
    ProductDetailSection *section4 = [ProductDetailSection MR_createEntity];
    section4.sectionTitle = @"Quantity:";
    section4.sectionCellQuantity = @(1);
    section4.sectionOrder = @(3);
    section4.sectionIdentifier = @"countProduct";
    section4.sectionRequired = @(YES);
    
    ProductDetailSection *section41 = [ProductDetailSection MR_createEntity];
    section41.sectionTitle = @"Quantity in Basket:";
    section41.sectionCellQuantity = @(1);
    section41.sectionOrder = @(4);
    section41.sectionIdentifier = @"quantityInBasket";
    section41.sectionRequired = @(NO);
#warning В режиме редактирования при установке этого флага сбивается порядок, что приводит к заполнению не того поля. надо бы это дело исправить.
    section41.sectionShowOnlyUser = @(NO);
    
    ProductDetailSection *section5 = [ProductDetailSection MR_createEntity];
    section5.sectionTitle = @"Lenght (m):";
    section5.sectionCellQuantity = @(1);
    section5.sectionOrder = @(5);
    section5.sectionIdentifier = @"lenght";
    
    ProductDetailSection *section51 = [ProductDetailSection MR_createEntity];
    section51.sectionTitle = @"Crew (unit):";
    section51.sectionCellQuantity = @(1);
    section51.sectionOrder = @(6);
    section51.sectionIdentifier = @"crew";
    
    ProductDetailSection *section6 = [ProductDetailSection MR_createEntity];
    section6.sectionTitle = @"Autonomy (standart year):";
    section6.sectionCellQuantity = @(1);
    section6.sectionOrder = @(7);
    section6.sectionIdentifier = @"autonomy";
    
    ProductDetailSection *section7 = [ProductDetailSection MR_createEntity];
    section7.sectionTitle = @"Identifier:";
    section7.sectionCellQuantity = @(1);
    section7.sectionOrder = @(9);
    section7.sectionIdentifier = @"identifier";
    section7.sectionShowOnlyAdmin = @(YES);
    section7.sectionRequired = @(YES);
    
    ProductDetailSection *section8 = [ProductDetailSection MR_createEntity];
    section8.sectionTitle = @"Tonnage (t):";
    section8.sectionCellQuantity = @(1);
    section8.sectionOrder = @(6);
    section8.sectionIdentifier = @"tonnage";
    
    ProductDetailSection *section9 = [ProductDetailSection MR_createEntity];
    section9.sectionTitle = @"URL address:";
    section9.sectionCellQuantity = @(1);
    section9.sectionOrder = @(10);
    section9.sectionIdentifier = @"urlAddress";
    section9.sectionShowOnlyAdmin = @(YES);

    // Cохраняем Managed Object Context
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreWithCompletion:nil];
}

@end
