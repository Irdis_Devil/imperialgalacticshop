//
//  IGSProductDetailVC.h
//  ImperialGalacticShop
//
//  Created by Алексей on 13.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface IGSProductDetailVC : UITableViewController

@property (strong,nonatomic) Product *localObject;
@property (assign,nonatomic) BOOL isAdminMode;
@property (assign,nonatomic) BOOL isAdminCreateProductMode;

@end
