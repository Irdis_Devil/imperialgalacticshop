//
//  IGSProductDetailVC.m
//  ImperialGalacticShop
//
//  Created by Алексей on 13.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSProductDetailVC.h"
#import "IGSConfigurateProductDetail.h"
#import "ProductDetailSection.h"
#import "UITextField+ConfigurateAtMode.h"
#import "IGSOperation.h"
#import "IGSSingleton.h"
#import "TPKeyboardAvoidingScrollView.h"

//идентификатор хэдера и ячейки
static NSString *simpleSectionHeader = @"simpleHeader";
static NSString *simpleSectionCell = @"simple";

//тэги для элементов жэдера и ячейки так же совпадают
#define SECTION_FIRST_TAG 111
#define SECTION_ONE_RED_STAR_TAG 211

//некоторые макросы, которые упростили жизнь
#define GET_SECTION_ENTITY_AT_INDEX(index) _localSectionDescription[(index)] //просто получаем секцию по индексу
#define CONVERT_TO_STRING(object) [object description] //конвертирует информацию для текстовых полей
#define CHECK_TO_DRAW_RED_STAR_MARK [[GET_SECTION_ENTITY_AT_INDEX(indexPath.section) sectionRequired] boolValue] && _isAdminMode // проверяет - нужно ли рисовать звездочку обязательного поля
#define GET_PRODUCT_PROPERTY_CLASS_NAME_AT_SECTION_IDENTIFIER(idf) [[[[Product entityDescription]propertiesByName]valueForKey:idf]attributeValueClassName] //получает имя класса свойства продукта по идентификатору


@interface IGSProductDetailVC () <UITextFieldDelegate>
{
    UITapGestureRecognizer *_tapGestureRecognizer;
    BOOL _isKeyboardVisible;
    NSArray *_localSectionDescription;
    NSMutableDictionary *_changedSectionsOrder;
}
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@end

@implementation IGSProductDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    _changedSectionsOrder = [NSMutableDictionary dictionary];
    [self.productImageView setImage:[IGSHelper getCurrentImageAtName:_localObject.imageName]];
    if (_isAdminMode)[self prepareNavBar];
    _localSectionDescription = [IGSConfigurateProductDetail getCurrentSectionAtMode:_isAdminMode andProduct:_localObject];
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapAnywhere:)];
}

-(void) prepareNavBar
{
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonWasPresed)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonWasPresed)];
    [self.navigationItem setLeftBarButtonItem:cancelButton];
    [self.navigationItem setRightBarButtonItem:doneButton];
    [self.navigationItem setTitle:(_isAdminCreateProductMode) ? @"Create new" : @"Edit"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    _isKeyboardVisible = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _localSectionDescription.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[GET_SECTION_ENTITY_AT_INDEX(section) sectionCellQuantity]integerValue];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: можно было бы это тоже записывать в секцию
    return 24;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [self.tableView dequeueReusableCellWithIdentifier:simpleSectionHeader];
    UILabel *firstLabel = (UILabel*)[view viewWithTag:SECTION_FIRST_TAG];
    firstLabel.text = [_localSectionDescription[section] sectionTitle];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleSectionCell forIndexPath:indexPath];
    [cell setTag:[[GET_SECTION_ENTITY_AT_INDEX(indexPath.section) sectionOrder] integerValue]];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(void)configureCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    UITextField *textField = (UITextField*)[cell viewWithTag:SECTION_FIRST_TAG];
    [self configurateTextField:textField withPropertyName:[GET_SECTION_ENTITY_AT_INDEX(indexPath.section) sectionIdentifier]];
    if (CHECK_TO_DRAW_RED_STAR_MARK)[[cell viewWithTag:SECTION_ONE_RED_STAR_TAG]setHidden:NO];
    //если убрать else, тогда могут некорректно прорисовывать звездочки
    else[[cell viewWithTag:SECTION_ONE_RED_STAR_TAG]setHidden:YES];
    textField.text = ([_changedSectionsOrder objectForKey:@(indexPath.section)] !=nil) ? [_changedSectionsOrder objectForKey:@(indexPath.section)] : CONVERT_TO_STRING([_localObject valueForKey:[GET_SECTION_ENTITY_AT_INDEX(indexPath.section) sectionIdentifier]]);
}

#pragma mark -
#pragma mark Keyboard handler

-(void)keyboardDidShow:(NSNotification *)notif
{
    if (_isKeyboardVisible) return;
    [self.tableView addGestureRecognizer:_tapGestureRecognizer];
    _isKeyboardVisible = YES;
}

-(void)keyboardDidHide:(NSNotification *)notif
{
    if (!_isKeyboardVisible)return;
    [self.tableView removeGestureRecognizer:_tapGestureRecognizer];
    _isKeyboardVisible = NO;
}

-(void)didTapAnywhere:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}


#pragma mark -
#pragma mark - Work with Text Fields

-(void)configurateTextField:(UITextField*)textField withPropertyName:(NSString*)propertyName
{
    textField.delegate = self;
    [textField addTarget:self action:@selector(writeSectionAtChangedTextField:) forControlEvents:UIControlEventEditingChanged];
    if (_isAdminMode) {
        //для полля будет установлена клавиатура в зависимости от типа
        [textField setAdminSettingsAndSetKeyBoardTypeAtClassName:GET_PRODUCT_PROPERTY_CLASS_NAME_AT_SECTION_IDENTIFIER(propertyName)];
    } else {
        [textField setUserSettings];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)writeSectionAtChangedTextField:(UITextField*)textfield
{
    NSNumber *orderChaged = [NSNumber numberWithInteger:[textfield.superview.superview.superview tag]];
    [_changedSectionsOrder setValue:([GET_PRODUCT_PROPERTY_CLASS_NAME_AT_SECTION_IDENTIFIER([GET_SECTION_ENTITY_AT_INDEX(orderChaged.integerValue) sectionIdentifier]) isEqualToString:NSStringFromClass([NSNumber class])]) ? @(textfield.text.floatValue): textfield.text forKey:[GET_SECTION_ENTITY_AT_INDEX(orderChaged.intValue) sectionIdentifier]];
}
#pragma mark - 
#pragma mark - preSave methods
//уличная магия, блин..

-(BOOL)checkFillRequiredsFields
{
#warning Написать проверку на каждое обязательное поле. количество может быть и нулевым, а вот с ценой как-то не так получается. Бесплатно раздавтаь что-ли?
    BOOL result = YES;
    for (ProductDetailSection *prDS in _localSectionDescription) {
        if (prDS.sectionRequired){
            UITableViewCell *tempCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:prDS.sectionCellQuantity.intValue-1 inSection:prDS.sectionOrder.intValue]];
            UITextField *oneTF = (UITextField*)[tempCell viewWithTag:SECTION_FIRST_TAG];
            if ([oneTF.text isEqualToString:@""]) result = NO;
        }
    }
    return result;
}

#pragma mark -
#pragma mark - Buttons handler

-(void)cancelButtonWasPresed
{
    if (_isAdminCreateProductMode)[_localObject MR_deleteEntity];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doneButtonWasPresed
{
    if ([self checkFillRequiredsFields]) {
        if (_changedSectionsOrder.count >0){
            IGSOperation *newOperation = [IGSOperation new];
            newOperation.operationKey = @"ChangeOrCreate";
            newOperation.product = _localObject;
            newOperation.parameters = _changedSectionsOrder;
            [[IGSSingleton sharedInstanse]addNewOperationInOperationStack:newOperation];
            [IGSMediator runOperationBlock];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        }//[IGSMediator changeProductParametersOrCreateAtProductIdentifier:_localObject.identifier dictionaryWithParametes:changedSectionsOrder];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *noFillFields = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"You are not fill required fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noFillFields show];
    }
}

@end
