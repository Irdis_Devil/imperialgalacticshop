//
//  IGSSingleton.h
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 19.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IGSOperation.h"

@interface IGSSingleton : NSObject

+(IGSSingleton*)sharedInstanse;

-(void)addNewOperationInOperationStack:(IGSOperation*)operation;
-(BOOL)isAllOperationDone;
-(dispatch_queue_t)getSingletonQueue;
-(NSInteger)getOperationArrayCount;
-(IGSOperation*)getFirstOperationFromStack;

@end
