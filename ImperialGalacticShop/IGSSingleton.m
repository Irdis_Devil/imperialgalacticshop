//
//  IGSSingleton.m
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 19.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "IGSSingleton.h"
#import "Product.h"

@interface IGSSingleton ()

@property (strong,nonatomic) dispatch_queue_t queue;
@property (strong,nonatomic) NSMutableArray *operationArray;
@property (assign,nonatomic) BOOL allOperationDone;
@property (strong,nonatomic) UIPopoverController *errorPopover;

@end

@implementation IGSSingleton

+(IGSSingleton *)sharedInstanse
{
    static IGSSingleton *singleton = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (singleton == nil)
            singleton=[[super allocWithZone:NULL]init];
    });
    
    return singleton;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return [self sharedInstanse];
}

-(id)init
{
    if (self = [super init])
    {
        self.queue = dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL);
        self.operationArray = [NSMutableArray array];
        self.allOperationDone = YES;
    }
    return self;
}

-(void)addNewOperationInOperationStack:(IGSOperation *)operation
{
    [[IGSSingleton sharedInstanse].operationArray addObject:operation];
}

-(BOOL)isAllOperationDone
{
    return [IGSSingleton sharedInstanse].allOperationDone;
}

-(dispatch_queue_t)getSingletonQueue
{
    return [IGSSingleton sharedInstanse].queue;
}

-(NSInteger)getOperationArrayCount
{
    return [IGSSingleton sharedInstanse].operationArray.count;
}

-(IGSOperation *)getFirstOperationFromStack
{
    [IGSSingleton sharedInstanse].allOperationDone = NO;
    IGSOperation *returningOperation = [[IGSOperation alloc] initWithOperation:[IGSSingleton sharedInstanse].operationArray.firstObject];
    [[IGSSingleton sharedInstanse].operationArray removeObjectAtIndex:0];
    if ([self getOperationArrayCount] == 0) [IGSSingleton sharedInstanse].allOperationDone = YES;
    return returningOperation;
}

@end
