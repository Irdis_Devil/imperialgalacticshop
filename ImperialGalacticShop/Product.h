//
//  Product.h
//  ImperialGalacticShop
//
//  Created by Администратор on 5/14/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Product : NSManagedObject <NSCoding>

@property (nonatomic, retain) NSNumber * autonomy;
@property (nonatomic, retain) NSNumber * countProduct;
@property (nonatomic, retain) NSNumber * crew;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * imageName;
@property (nonatomic, retain) NSNumber * lenght;
@property (nonatomic, retain) NSString * manufacturer;
@property (nonatomic, retain) NSString * nameProduct;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSNumber * quantityInBasket;
@property (nonatomic, retain) NSNumber * tonnage;
@property (nonatomic, retain) NSString * urlAddress;

@end
