//
//  Product.m
//  ImperialGalacticShop
//
//  Created by Администратор on 5/14/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "Product.h"

#define AUTONOMY @"autonomy"
#define COUNTPRODUCT @"countProduct"
#define CREW @"crew"
#define IDENTIFIER @"identifier"
#define IMAGENAME @"imageName"
#define LENGHT @"lenght"
#define MANUFACTURER @"lenght"
#define NAMEPRODUCT @"nameProduct"
#define PRICE @"price"
#define QUANTITYINBASKET @"quantityInBasket"
#define TONNAGE @"tonnage"
#define URLADDRESS @"urlAddress"

@implementation Product 

@dynamic autonomy;
@dynamic countProduct;
@dynamic crew;
@dynamic identifier;
@dynamic imageName;
@dynamic lenght;
@dynamic manufacturer;
@dynamic nameProduct;
@dynamic price;
@dynamic quantityInBasket;
@dynamic tonnage;
@dynamic urlAddress;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.nameProduct = [aDecoder decodeObjectForKey:NAMEPRODUCT];
        self.manufacturer = [aDecoder decodeObjectForKey:MANUFACTURER];
        self.countProduct = [aDecoder decodeObjectForKey:COUNTPRODUCT];
        self.crew = [aDecoder decodeObjectForKey:CREW];
        self.imageName = [aDecoder decodeObjectForKey:IMAGENAME];
        self.lenght = [aDecoder decodeObjectForKey:LENGHT];
        self.price = [aDecoder decodeObjectForKey:PRICE];
        self.autonomy = [aDecoder decodeObjectForKey:AUTONOMY];
        self.quantityInBasket = [aDecoder decodeObjectForKey:QUANTITYINBASKET];
        self.tonnage = [aDecoder decodeObjectForKey:TONNAGE];
        self.urlAddress = [aDecoder decodeObjectForKey:URLADDRESS];
        self.identifier = [aDecoder decodeObjectForKey:IDENTIFIER];
    }
    return self;

}
 -(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.nameProduct forKey:NAMEPRODUCT];
    [aCoder encodeObject:self.manufacturer forKey:MANUFACTURER];
    [aCoder encodeObject:self.countProduct forKey:COUNTPRODUCT];
    [aCoder encodeObject:self.crew forKey:CREW];
    [aCoder encodeObject:self.imageName forKey:IMAGENAME];
    [aCoder encodeObject:self.lenght forKey:LENGHT];
    [aCoder encodeObject:self.price forKey:PRICE];
    [aCoder encodeObject:self.autonomy forKey:AUTONOMY];
    [aCoder encodeObject:self.quantityInBasket forKey:QUANTITYINBASKET];
    [aCoder encodeObject:self.tonnage forKey:TONNAGE];
    [aCoder encodeObject:self.urlAddress forKey:URLADDRESS];
    [aCoder encodeObject:self.identifier forKey:IDENTIFIER];
}
@end
