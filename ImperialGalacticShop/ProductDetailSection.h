//
//  ProductDetailSection.h
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 16.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ProductDetailSection : NSManagedObject

@property (nonatomic, retain) NSNumber * sectionCellQuantity;
@property (nonatomic, retain) NSString * sectionIdentifier;
@property (nonatomic, retain) NSNumber * sectionOrder;
@property (nonatomic, retain) NSNumber * sectionRequired;
@property (nonatomic, retain) NSString * sectionTitle;
@property (nonatomic, retain) NSNumber * sectionShowOnlyAdmin;
@property (nonatomic, retain) NSNumber * sectionShowOnlyUser;
@property (nonatomic, retain) NSString * sectionTemporaryStorage;

@end
