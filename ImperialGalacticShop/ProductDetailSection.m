//
//  ProductDetailSection.m
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 16.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "ProductDetailSection.h"


@implementation ProductDetailSection

@dynamic sectionCellQuantity;
@dynamic sectionIdentifier;
@dynamic sectionOrder;
@dynamic sectionRequired;
@dynamic sectionTitle;
@dynamic sectionShowOnlyAdmin;
@dynamic sectionShowOnlyUser;
@dynamic sectionTemporaryStorage;

@end
