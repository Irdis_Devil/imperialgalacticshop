//
//  ShopCell.h
//  ImperialGalacticShop
//
//  Created by Алексей on 13.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShopCellDelegate <NSObject>

-(void)stepperChangeValueInCell:(UITableViewCell*)cell;
-(void)stepperValueIsNullinCell:(UITableViewCell*)cell;

@end

@interface ShopCell : UITableViewCell

@property (weak, nonatomic) id <ShopCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *manufacturerLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextField *countToBasket;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UIButton *showDetailButton;

-(void)setMaxValueToStepper:(double)maxValue;

@end
