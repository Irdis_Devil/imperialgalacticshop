//
//  ShopCell.m
//  ImperialGalacticShop
//
//  Created by Алексей on 13.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "ShopCell.h"

@implementation ShopCell

-(void)awakeFromNib
{
    self.countToBasket.text = [NSString stringWithFormat:@"%i",(int)self.stepper.value];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(IBAction)valueChanged:(UIStepper *)sender
{
    self.countToBasket.text = self.countToBasket.text = [NSString stringWithFormat:@"%i",(int)self.stepper.value];
    [self.delegate performSelector:@selector(stepperChangeValueInCell:) withObject:self];
    if (self.stepper.value == 0)
    {
        [self.delegate performSelector:@selector(stepperValueIsNullinCell:) withObject:self];
//        [self setSelected:NO animated:YES];
//        [self setAccessoryType:UITableViewCellAccessoryNone];
    }
}

-(void)setMaxValueToStepper:(double)maxValue
{
    self.stepper.maximumValue = maxValue;
}

@end
