//
//  ShopCellEntity.h
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 19.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ShopCellEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * cellSelected;
@property (nonatomic, retain) NSNumber * stepperValue;
@property (nonatomic, retain) NSNumber * indexRow;

@end
