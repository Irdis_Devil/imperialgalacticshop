//
//  ShopCellEntity.m
//  ImperialGalacticShop
//
//  Created by Алексей Борисов on 19.05.14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "ShopCellEntity.h"


@implementation ShopCellEntity

@dynamic cellSelected;
@dynamic stepperValue;
@dynamic indexRow;

@end
