//
//  UITextField+ConfigurateAtMode.h
//  ImperialGalacticShop
//
//  Created by Администратор on 5/15/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (ConfigurateAtMode)

-(void)setAdminSettingsAndSetKeyBoardTypeAtClassName:(NSString*)className;
-(void)setUserSettings;

@end
