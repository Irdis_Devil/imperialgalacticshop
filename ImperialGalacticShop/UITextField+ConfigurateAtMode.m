//
//  UITextField+ConfigurateAtMode.m
//  ImperialGalacticShop
//
//  Created by Администратор on 5/15/14.
//  Copyright (c) 2014 GalacticEmpire. All rights reserved.
//

#import "UITextField+ConfigurateAtMode.h"

@implementation UITextField (ConfigurateAtMode)

-(void)setAdminSettingsAndSetKeyBoardTypeAtClassName:(NSString *)className
{
    [self setBorderStyle:UITextBorderStyleRoundedRect];
    [self setUserInteractionEnabled:YES];
    //у нас там еще есть УРЛ адрес, неплохо было бы для него клавиатурку специальную сделать
    if ([className isEqualToString:NSStringFromClass([NSNumber class])])
        [self setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
}

-(void)setUserSettings
{
    [self setBorderStyle:UITextBorderStyleNone];
    [self setUserInteractionEnabled:NO];
}

@end
